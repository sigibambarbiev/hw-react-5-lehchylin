import { Outlet, Link } from "react-router-dom";
import s from "./layout.module.scss"

export function Layout () {
    return (
        <>
            <div className={s.layout}>
                <li >
                    <Link className={s.link} to="/">Home</Link>
                </li>
                <li>
                    <Link className={s.link} to="favorite">Favorites</Link>
                </li>
                <li>
                    <Link className={s.link} to="cart">Cart</Link>
                </li>
            </div>
                <Outlet />
        </>
    )
}
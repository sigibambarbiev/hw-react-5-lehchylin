import React from 'react';
import {useSelector} from "react-redux";

import {Product} from "./products/product";
import {Form} from "../UI/Form/form";
import s from "./cart.module.scss";


export function Cart(props) {

    const {onButton, removeButtonCart, counterCartMinus} = props;
    const products = useSelector(store => store.productList);
    const cart = useSelector(store => store.cart);

    function PaintProductCard(product) {

        const {name, price, url, number, volume} = product;

        return (
            <>
                <Product
                    key={number}
                    name={name}
                    price={price}
                    url={url}
                    number={number}
                    volume={volume}
                    onButton={onButton}
                    removeButtonCart={removeButtonCart}
                    counterCartMinus={counterCartMinus}
                />
            </>
        )
    }

    return (
        <div>
            <div className={s.header}>
                <p className={s.title}>CART</p>
            </div>
            <div className={s.wrapper}>
                <div className={s.productWrapp}>
                    {products.map(product => cart.includes(product.number) && PaintProductCard(product))}
                </div>
                <Form />
            </div>
        </div>
    )

}

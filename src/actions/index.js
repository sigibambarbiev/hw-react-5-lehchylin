import {request} from "../tools/request";

const API_URL = './drinks.json';

export const GET_DRINKS = "GET_DRINKS";
export const GET_MODAL_STATUS = "GET_MODAL_STATUS";
export const CHANGE_MODAL_STATUS_TRUE = "CHANGE_MODAL_STATUS_TRUE";
export const CHANGE_MODAL_STATUS_FALSE = "CHANGE_MODAL_STATUS_FALSE";

export const GET_CART = "GET_CART";
export const ADD_TO_CART = "ADD_TO_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";
export const CLEAR_CART = "CLEAR_CART";
export const GET_ON_CART_STATUS = "GET_ON_CART_STATUS";
export const CHANGE_ON_CART_STATUS_TRUE = "CHANGE_ON_CART_STATUS_TRUE";
export const CHANGE_ON_CART_STATUS_FALSE = "CHANGE_ON_CART_STATUS_FALSE";
export const CLEAR_CART_STATUS = "CLEAR_CART_STATUS";

export const ADD_TO_FAVORITES = "ADD_TO_FAVORITES";
export const REMOVE_FROM_FAVORITES = "REMOVE_FROM_FAVORITES";

const actionDrinks = (response) => ({type: GET_DRINKS, payload: response});
const actionModalStatus = (modalStatus) => ({type: GET_MODAL_STATUS, payload: modalStatus});
const actionModalTrue = (number) => ({type: CHANGE_MODAL_STATUS_TRUE, payload: number});
const actionModalFalse = (number) => ({type: CHANGE_MODAL_STATUS_FALSE, payload: number});

const actionGetCart = (cartArray) => ({type: GET_CART, payload: cartArray});
const actionAddCart = (number) => ({type: ADD_TO_CART, payload: number});
const actionRemoveCart = (number) => ({type: REMOVE_FROM_CART, payload: number});
const actionClearCartStatus = () => ({type: CLEAR_CART_STATUS});
const actionClearCart = () => ({type: CLEAR_CART});
const actionOnCartStatus = (onCartStatus) => ({type: GET_ON_CART_STATUS, payload: onCartStatus});
const actionOnCartStatusTrue = (number) => ({type: CHANGE_ON_CART_STATUS_TRUE, payload: number});
const actionOnCartStatusFalse = (number) => ({type: CHANGE_ON_CART_STATUS_FALSE, payload: number});
// export const actionModalTrue = (number) => ({type: CHANGE_MODAL_STATUS_TRUE, payload: number});
// export const actionModalFalse = (number) => ({type: CHANGE_MODAL_STATUS_FALSE, payload: number});

export const ModalTrue = (number) => (dispatch) => {
    dispatch(actionModalTrue(number))
};

export const ModalFalse = (number) => (dispatch) => {
    dispatch(actionModalFalse(number))
};

export const GetCart =() => (dispatch) => {
    const getCart = JSON.parse(localStorage.getItem('cartArray'));
    const cart = [];
    for (const key in getCart) {
        cart.push(getCart[key])
    }
    dispatch(actionGetCart(cart))
};

export const AddCart = (number) => (dispatch) => {
        dispatch(actionAddCart(number))
};

export const RemoveCart = (number) => (dispatch) => {
    dispatch(actionRemoveCart(number))
};

export const ClearCart = () => (dispatch) => {
    dispatch(actionClearCart())
};

export const ClearCartStatus = () => (dispatch) => {
    dispatch(actionClearCartStatus())
};

export const OnCartStatusTrue = (number) => (dispatch) => {
    dispatch(actionOnCartStatusTrue(number))
};

export const OnCartStatusFalse = (number) => (dispatch) => {
    dispatch(actionOnCartStatusFalse(number))
};

export const fetchAllProducts = () => async (dispatch) => {
    const response = await request(API_URL)

    if (response) {
        dispatch(actionDrinks(response));

        const modalStatus = {};
        const onCartStatus = {};
        response.forEach(item => {
            for (const key in item) {
                if (key === "number") {
                    modalStatus[item[key]] = false;
                    onCartStatus[item[key]] = false;
                }
            }
        });
        dispatch(actionModalStatus(modalStatus));
        dispatch(actionOnCartStatus(onCartStatus));
    }
}
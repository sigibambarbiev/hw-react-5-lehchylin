import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Routes, Route} from "react-router-dom";

import {ProductList} from "./pages/products/productList";
import {Cart} from "./pages/cart";
import {Favorite} from "./pages/favorite";
import {Layout} from "./pages/layout";
import {AddCart, fetchAllProducts, GetCart, RemoveCart} from "./actions";
import s from './App.module.scss';

export function App() {
    const dispatch = useDispatch();

    const cart = useSelector(store => store.cart);
    const [favorites, setFavorites] = useState([]);
    const [counterStar, setCounterStar] = useState(0);
    const [counterCart, setCounterCart] = useState(0);

    useEffect(() => {
        dispatch(fetchAllProducts());
    }, []);

    // localStorage.removeItem('cartArray')

    useEffect(() => {
        dispatch(GetCart())
    }, []);

    useEffect(() => {
        localStorage.setItem('cartArray', JSON.stringify(cart));
    }, [cart]);

    useEffect(() => {
        setFavorites(JSON.parse(localStorage.getItem('starArray')));
    }, []);

    useEffect(() => {
        localStorage.setItem('starArray', JSON.stringify(favorites));
    }, [favorites]);

    useEffect(() => {
        localStorage.setItem('counterCart', counterCart.toString());
    }, [counterCart]);

    useEffect(() => {
        localStorage.setItem('counterStar', counterStar.toString());
    }, [counterStar]);

    function AddToCart(number) {
        dispatch(AddCart(number))
    }

    function AddToFavorites(number) {
        setFavorites(prevState => [...prevState, number]);
    }

    function RemoveFromCart(number) {
        dispatch(RemoveCart(number))
    }

    function RemoveFromFavorites(number) {
        setFavorites(favorites.filter(elem => elem !== number));
    }

    function CounterCartMinus(number) {
        setCounterCart(counterCart - 1);
        RemoveFromCart(number);
    }

    function CounterStarMinus(number) {
        setCounterStar(counterStar - 1);
        RemoveFromFavorites(number);
    }

    return (
        <div className={s.app}>
            <Routes>
                <Route path="/" element={<Layout/>}>
                    <Route index element={<ProductList
                        addToCart={AddToCart}
                        addToFavorites={AddToFavorites}
                        counterCartMinus={CounterCartMinus}
                        counterStarMinus={CounterStarMinus}
                        onButton={true}
                        removeButtonCart={false}
                        removeButtonStar={false}
                        counterCart={counterCart}
                        setCounterCart={setCounterCart}
                        counterStar={counterStar}
                        setCounterStar={setCounterStar}
                    />}/>
                    <Route path="cart" element={<Cart
                        onButton={false}
                        removeButtonCart={true}
                        counterCartMinus={CounterCartMinus}
                        counterCart={counterCart}
                        setCounterCart={setCounterCart}
                    />}/>
                    <Route path="favorite" element={<Favorite
                        favorites={favorites}
                        onButton={false}
                        removeButtonStar={true}
                        counterStarMinus={CounterStarMinus}/>}/>
                </Route>
            </Routes>
        </div>
    )
}


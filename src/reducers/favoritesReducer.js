//заготовка на будущее
import * as actions from "../actions"

const defaultState = [];

export function favoritesReducer(state = defaultState, action) {
    switch (action.type) {
        case actions.ADD_TO_FAVORITES:
            return [...state, action.payload];
        case actions.REMOVE_FROM_FAVORITES:
            return [...state, action.payload];
        default:
            return state
    }

}